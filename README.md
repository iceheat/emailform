# Email Form

An Email form builder package that allows the users to customise the templates and the fields through config files.

Utilising a config file, instead of an interface to build form classes, can provide a path to moving the configuration to thd database.

This methodology provides a radically different approach to the on used in `kris/laravel-form-builder`


## Install

Via Composer
add the following to your `composer.json` file
```
"repositories": [
{
    "type":"git",
    "url":"git@bitbucket.org:iceheat/emailform.git"
}
]
```
Then you can add it using
``` bash
$ composer require Ammar/EmailForm
```

#### If you are using a laravel version `< 5.5` :
* you will need to add the service provider to your `config/app.php`
```php
 Ammar\EmailForm\EmailFormServiceProvider::class,
```
* Optional: you can add the facade to your aliases section of your laravel `app.php`
```php
'EmailForm' =>  Ammar\EmailForm\EmailFormServiceProvider::class,
```

## Usage

You will need a messages table in your database to persist received messages. You can run 
``` php
php artisan migrate
```
to add the required table. I opted for a simple table with a text field for `messages` and a json field for `other_fields`

If you want to change the configuration of the package, you can use
```php
php artisan vendor:publish
```
to publish the configuration files and the views to your project where you can modify them.


```php
Ammar\EmailForm\Emailform::getTemplates();
```
There are two included templates `email_enquiry` and `personal_enquiry`
would return a list of the available templates that you can use. 
This can also be viewed through the inspection of `emailForm.php` config file 

#### generating a new form
A new form can be generated using 
```php
$form = new EmailForm()
```
The constructor for `EmailForm` can take an argument of the template
alternatively the template can be set using 
```php
$form->setType('email_enquiry');
```

To generate a rendered view of the form
```php 
$form->view();
```
can be used.

#### Processing the form

You will need to create an `EmailForm` instance with the correct template to manipulate the form

You can either set the type manually, or you can use the data from the form to set the type. 
The generated form includes a hidden `form_template` field that holds the template name.

```php
 $form = new EmailForm($request->get('form_template'));
```
would get you the correct form type.

1. `$form->setData($request->all());` would automatically filter the data, validate it and set the data to the model.
2. you can use `$form->store()` to persist the data to the database. 

    ** 1 & 2 can be combined by passing the data as a parameter to `store` method.
3. you can use `$form->email('foo@example.com')` to queue and email to be sent to about the form. 
omitting the sender address would result on the method using the default to address of the application.

*** all these methods can be chained  `$form->setData($request->all())->store()->email`

A sample implementation can be accessed through `<laravel-domain>/form`