<?php

namespace Ammar\EmailForm;
use Ammar\EmailForm\Events\EmailFormSubmitted;
use Ammar\EmailForm\Listeners\SendFormToEmail;
use Ammar\EmailForm\Listeners\StoreFormInDatabase;
use Event;
use Illuminate\Support\ServiceProvider;

class EmailFormServiceProvider extends ServiceProvider
{

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->loadViewsFrom(__DIR__.'/views','emailForms');
        $this->publishes([
            __DIR__.'/views'=>resource_path('views/vendor/emailForms'),
            __DIR__.'/config/emailForm.php'=>config_path('emailForm.php')
        ]);

//        Event::listen(EmailFormSubmitted::class,StoreFormInDatabase::class);
//        Event::listen(EmailFormSubmitted::class,SendFormToEmail::class);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/emailForm.php', 'emailForm'
        );
    }
}