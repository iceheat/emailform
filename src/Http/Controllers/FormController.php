<?php

namespace Ammar\EmailForm\Http\Controllers;

use Ammar\EmailForm\EmailForm;
use Ammar\EmailForm\Events\EmailFormSubmitted;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Event;

class FormController extends Controller
{
    /**
     * @param Request $request
     * @param EmailForm $form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Emailform $form){
        $form->setType('personal_enquiry');
        return view('emailForms::formSample', ['form'=>$form]);

    }

    public function store(Request $request)
    {
        //        event(new EmailFormSubmitted($form));
        $form = new EmailForm($request->get('form_template'));
        $form->store($request->all());
        return redirect('/form');


    }
}
