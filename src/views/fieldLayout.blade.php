
<div class="form-group @if(!empty($errors)) has-error @endif">
    <label for="{{$name}}">{{$label}}</label>

    @if ($type == "textarea")
        <textarea class="form-control  " rows="5" id="{{$name}}" name="{{$name}}"></textarea>
    @else
        <input type="{{$type}}" class="form-control" id="{{$name}}" name="{{$name}}">
    @endif

    @if(!empty($errors))
        <p class="alert-danger">
            @foreach ($errors as $error)
                {{ $error }}<br>
            @endforeach
        </p>
        @endif
</div>
