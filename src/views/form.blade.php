<form action="{{config('emailForm.postUrl')}}" method="POST">
    {!! csrf_field() !!}
    <input type="hidden" name="form_template" value={{$template}}>
    {{--{{var_dump(session('errors'))}}--}}
    @foreach($fields as $field)
        @include ('emailForms::fieldLayout',[
        'name'=>$field['name'],
        'type'=>$field['type'],
        'label'=>$field['description'],
        'errors'=>session('errors')->get($field['name'])
        ])
    @endforeach

    <button class="btn btn-primary">Submit the Form</button>

</form>