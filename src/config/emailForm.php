<?php
return [
    'postUrl'=>'/form',


    'templates' => [
        'email_enquiry' => [
            'fields' => ['email', 'message']
        ],
        'personal_enquiry'=>[
            'fields'=>['first_name','last_name','message']
        ]

    ],


    'fields' => [
        'first_name' => [
            'type' => 'text',
            'description' => 'First Name',
            'help' => 'Please enter your first name',
            'validation' => 'required|min:3',
        ],
        'last_name' => [
            'type' => 'text',
            'description' => 'Surname',
            'help' => 'Please enter your surname',
            'validation' => 'required|min:3',
        ],
        'email' => [
            'type' => 'email',
            'description' => 'Email',
            'help' => 'Please provide your email',
            'validation' => 'required|email',
        ],
        'message' => [
            'type' => 'textarea',
            'description' => 'Message',
            'help' => 'Please provide a message',
            'validation' => 'min:5',
        ],

    ]
];