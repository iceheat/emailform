<?php

namespace Ammar\EmailForm\Listeners;

use Ammar\EmailForm\Events\EmailFormSubmitted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendFormToEmail
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param EmailFormSubmitted|object $event
     * @return void
     */
    public function handle(EmailFormSubmitted $event)
    {
        dd($event);
    }
}
