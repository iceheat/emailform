<?php
/**
 * Created by PhpStorm.
 * User: Ammar
 * Date: 19/09/2017
 * Time: 15:02
 */

namespace Ammar\EmailForm;


use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $casts = [
        'other_fields' => 'array',
    ];
    protected $fillable = ['message','other_fields'];
}