<?php
/**
 * Created by PhpStorm.
 * User: Ammar
 * Date: 19/09/2017
 * Time: 15:02
 */

namespace Ammar\EmailForm;


use Ammar\EmailForm\Mail\FormSubmitted;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\MessageBag;
use Mail;
use Session;
use View;

class EmailForm
{
    /**
     * EmailForm constructor.
     * @param null $template
     */
    private $template;
    private $fields;
    private $data;
    private $validator;


    /**
     * EmailForm constructor.
     * @param string $template
     */
    public function __construct($template = 'email_enquiry')
    {
        $this->setType($template);
    }


    public function setType($template)
    {

        $this->template = $template;
        $this->fields = $this->getFields($template);

        return $this;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param null $errors
     */
    public function view()
    {

        $data['fields'] = $this->fields;
        $data['template'] = $this->template;
        $data['errors'] = $this->validator->errors ?? new MessageBag();

        $formView = View::make('emailForms::form', $data)->render();

        return $formView;
    }

    /**
     * @return array
     */
    public static function getTemplates(){
        return array_keys(config('emailForm.templates'));
    }

    /**
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        //ensure that we do not have any undesired inputs fields
        foreach ($data as $key => $value) {
            $this->data[$key] = $value ?? null;
        }
        $this->validate();
        return $this;

    }

    /**
     * @param array|null $data
     * @return $this
     */
    public function store(array $data = null)
    {

        if (!empty($data))
            $this->setData($data);

        $data = $this->data;

        Session::remove('errors');

        if ($this->isValid()) {
            $messageText = $data['message'];
            unset($data['message']);

            $message = new Message();
            $message->message = $messageText;
            $message->other_fields = $data;
            $message->save();

        }
        else {

            Session::put(['errors'=>$this->validator->errors()]);
//            dd(session('form_errors'));
        };
        return $this;

    }

    /**
     * @param null $emailAddress
     * @return $this
     */
    public function email($emailAddress = null)
    {

        if($emailAddress)
            Mail::to($emailAddress)->queue(new FormSubmitted($this));
        else
            Mail::queue(new FormSubmitted($this->data));

        return $this;
    }

    /**
     *
     */
    private function validate()
    {
        $validators = [];
        foreach ($this->fields as $field) {
            $validators[$field['name']] = $field['validation'];
        }
        $this->validator = \Validator::make($this->data, $validators);
        return $this->validator;

    }




    /**
     * @param $template
     * @return array
     */
    private function getFields($template)
    {
        $fields = [];
        foreach (config("emailForm.templates.$template.fields") as $name) {
            $fields[] = config("emailForm.fields.$name") + ['name' => $name];

        };
        return $fields;
    }

    /**
     * @return bool
     */
    private function isValid()
    {
        return empty($this->validate()->errors()->all());
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __toString()
    {
        return $this->view();
    }

}