<?php
/**
 * Created by PhpStorm.
 * User: Ammar
 * Date: 19/09/2017
 * Time: 13:10
 */


Route::group(['namespace'=>'Ammar\\EmailForm\\Http\\Controllers', 'middleware'=>['web']], function(){
    Route::get('form', 'FormController@index');
    Route::post('form', 'FormController@store');
});
